import json
import math
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime

SHOW_TARGET_RECOMPUTATION = False

with open('data.json') as f:
    data = json.load(f)

with open('data_constant_difficulty.json') as f:
    dataConstantDifficulty = json.load(f)

with open('data_keeping_all_blocks_since_last_m_higher_block.json') as f:
    dataKeepingAllBlocksSinceLastMHigherBlock = json.load(f)

fig, targetsAxis = plt.subplots()
compressedSizeAxis = targetsAxis.twinx()
compressedScoreAxis = targetsAxis.twinx()
axes = [targetsAxis, compressedSizeAxis, compressedScoreAxis, targetsAxis.twinx(), compressedScoreAxis, compressedSizeAxis, compressedScoreAxis]

targets = data['targets']

timestamps = [datetime.datetime.fromtimestamp(timestamp) for timestamp in data['timestamps']]

for x, (target, nextTarget) in enumerate(zip(targets, targets[1:])):
    if nextTarget < target:
        firstTargetDecreaseCurve = plt.axvline(x = timestamps[x], color = 'purple', label = 'first target decrease')
        break

targetRecomputationCurve = []
if SHOW_TARGET_RECOMPUTATION:
    for x in range(0, len(targets), 2016):
        targetRecomputationCurve = [plt.axvline(x = timestamps[x], alpha = 0.1, label = 'target recomputation')]

for x, (target, nextTarget) in enumerate(zip(targets, targets[1:])):
    if nextTarget > target:
        targetIncreaseCurve = plt.axvline(x = timestamps[x], alpha = 0.5, color = 'red', label = 'target increase')

axesColors = ['blue', 'green', 'brown', 'orange', 'brown', 'red', 'purple']
axesLabels = ['targets', 'compressed blockchain size (in blocks)', 'compressed blockchain score', 'constant difficulty compressed blockchain size (in blocks)', 'constant difficulty compressed blockchain score', 'compressed blockchain size keeping all blocks since last m higher block (in blocks)', 'compressed blockchain score keeping all blocks since last m higher block']
axesYValues = [targets, data['compressSize'], data['compressScore'], dataConstantDifficulty['compressSize'], dataConstantDifficulty['compressScore'], dataKeepingAllBlocksSinceLastMHigherBlock['compressSize'], dataKeepingAllBlocksSinceLastMHigherBlock['compressScore']]

blockHeightsCache = {}

class CustomDateFormatter(mdates.ticker.Formatter):
    def __call__(self, x, pos=0):
        datetime_ = mdates.num2date(x)
        if not x in blockHeightsCache:
            for timestampsIndex, timestamp in enumerate(timestamps):
                if datetime_.timestamp() <= timestamp.timestamp():
                    blockHeightsCache[x] = timestampsIndex
                    break
        blockHeight = f' - {blockHeightsCache[x]}' if x in blockHeightsCache else ''
        result = datetime_.strftime('%m/%Y') + blockHeight
        return result

plt.gca().xaxis.set_major_formatter(CustomDateFormatter())

curves = []
for curvesIndex, (axis, color, label, yValues) in enumerate(zip(axes, axesColors, axesLabels, axesYValues)):
    dashes = (None, None) if curvesIndex != 4 else [1, 25]
    alpha = 1 if curvesIndex != 3 else 0.5
    # TODO: remove below, could maybe precise file names in scripts not to mess up
    # Due to not having exported `timestamps` for every execution:
    comparableLength = min(len(timestamps), len(yValues))
    curves += axis.plot(timestamps[:comparableLength], yValues[:comparableLength], label = label, color = color, dashes = dashes, alpha = alpha)

plt.gcf().autofmt_xdate()

targetsAxis.set_yscale('log', base=2)

axes[2].spines.right.set_position(('axes', 1.1))
axes[2].set_yscale('log')

axes[-2].spines.right.set_position(('axes', 1.2))

legendNewLine = targetsAxis.plot([], [], color='none', label=' ')
curves.insert(-2, legendNewLine[0])

curves += legendNewLine + targetRecomputationCurve + [firstTargetDecreaseCurve] + [targetIncreaseCurve]
labels = [curve.get_label() for curve in curves]

plt.legend(curves, labels, loc='upper center', framealpha=1, bbox_to_anchor=(0.5, -0.345))

targetsAxis.set_xlabel('Block date and height')

for ax, color in zip(axes, axesColors):
    ax.tick_params(axis='y', colors=color)

title = 'Variable difficulty Bitcoin compressed blockchain evolution'
plt.title(title)

fig.subplots_adjust(top=0.965,
                    bottom=0.525,
                    left=0.125,
                    right=0.8)

fig.set_size_inches((11.2, 7.5), forward=False)
plt.savefig(f'{title.lower().replace(" ", "_")}.svg')
plt.show()