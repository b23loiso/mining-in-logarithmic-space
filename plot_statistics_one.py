import json
import math
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime

SHOW_TARGET_RECOMPUTATION = False

with open('data.json') as f:
    data = json.load(f)

fig, targetsAxis = plt.subplots()
compressedScoreAxis = targetsAxis.twinx()
axes = [targetsAxis, targetsAxis.twinx(), compressedScoreAxis]

targets = data['targets']

timestamps = [datetime.datetime.fromtimestamp(timestamp) for timestamp in data['timestamps']]

for x, (target, nextTarget) in enumerate(zip(targets, targets[1:])):
    if nextTarget < target:
        firstTargetDecreaseCurve = plt.axvline(x = timestamps[x], color = 'purple', label = 'first target decrease')
        break

targetRecomputationCurve = []
if SHOW_TARGET_RECOMPUTATION:
    for x in range(0, len(targets), 2016):
        targetRecomputationCurve = [plt.axvline(x = timestamps[x], alpha = 0.1, label = 'target recomputation')]

for x, (target, nextTarget) in enumerate(zip(targets, targets[1:])):
    if nextTarget > target:
        targetIncreaseCurve = plt.axvline(x = timestamps[x], alpha = 0.5, color = 'red', label = 'target increase')

axesColors = ['blue', 'green', 'brown']
axesLabels = ['targets', 'compressed blockchain size (in blocks)', 'compressed blockchain score']
axesYValues = [targets, data['compressSize'], data['compressScore']]

blockHeightsCache = {}

class CustomDateFormatter(mdates.ticker.Formatter):
    def __call__(self, x, pos=0):
        datetime_ = mdates.num2date(x)
        if not x in blockHeightsCache:
            for timestampsIndex, timestamp in enumerate(timestamps):
                if datetime_.timestamp() <= timestamp.timestamp():
                    blockHeightsCache[x] = timestampsIndex
                    break
        blockHeight = f' - {blockHeightsCache[x]}' if x in blockHeightsCache else ''
        result = datetime_.strftime('%m/%Y') + blockHeight
        return result

plt.gca().xaxis.set_major_formatter(CustomDateFormatter())

curves = []
for curvesIndex, (axis, color, label, yValues) in enumerate(zip(axes, axesColors, axesLabels, axesYValues)):
    curves += axis.plot(timestamps, yValues, label = label, color = color)

plt.gcf().autofmt_xdate()

targetsAxis.set_yscale('log', base=2)

axes[2].spines.right.set_position(('axes', 1.1))
axes[2].set_yscale('log')

legendNewLine = targetsAxis.plot([], [], color='none', label=' ')

for curve in targetRecomputationCurve + [firstTargetDecreaseCurve, targetIncreaseCurve][::-1]:
    curves.insert(1, curve)
curves.insert(-2, legendNewLine[0])
labels = [curve.get_label() for curve in curves]

plt.legend(curves, labels, loc='upper center', framealpha=1, bbox_to_anchor=(0.5, -0.18))

fig.subplots_adjust(left = 0.035)
fig.subplots_adjust(right = 0.875)
fig.subplots_adjust(bottom = 0.32)
fig.subplots_adjust(top = 0.965)

targetsAxis.set_xlabel('Block height')

for ax, color in zip(axes, axesColors):
    ax.tick_params(axis='y', colors=color)

title = 'Variable difficulty Bitcoin compressed blockchain evolution'
plt.title(title)

plt.show()
fig.set_size_inches((11.2, 7.5), forward=False)
#plt.savefig(f'{title.lower().replace(" ", "_")}.svg')
