from bcolors import bcolors
import time
import bitcoin
from block import IS_EPSILON_TARGET_DEFINITION
import json
from argparser import get_parser
import config
import copy

# Bitcoin parameters
κ = 256

KEEPING_ALL_BLOCKS_SINCE_LAST_M_HIGHER_BLOCK = False

# Minimal target difference
sanityTargetDifferenceThreshold = 0.03408116026637664

def Dissolve(m, k, C, previous_score, previous_ℓ):
    CStar = C[:-k]
    D = {}
    if len(CStar) >= 2 * m:
        ℓ = getℓ(CStar, previous_ℓ)
        while True:
            # Could make a single function iterating on `CStar` filling a dict with keys being levels and values being the number of blocks of the associated level and stop once a level value as reached a given number of blocks.
            CIndex, μ = getFirstBlockExceedingBlocksPerLevel(CStar, 2 * m, ℓ)
            if CIndex == None:
                break
            removed, blockRemoved = llbracket(CStar, μ, CIndex)
            isScoreIncreasing = score(CStar, ℓ) + sanityTargetDifferenceThreshold >= previous_score
            if not isScoreIncreasing:
                if removed:
                    CStar.insert(CIndex, blockRemoved)
                else:
                    CStar[CIndex].level_min -= 1
                break
        for μ in range(ℓ + 1):
            D[μ] = uparrow(CStar, μ)
    else:
        ℓ = 0
        D[0] = CStar
    CStarScore = score(CStar, ℓ)
    if (IS_EPSILON_TARGET_DEFINITION and (previous_score - CStarScore) >= sanityTargetDifferenceThreshold) or (not IS_EPSILON_TARGET_DEFINITION and CStarScore < previous_score):
        print(f'score decreasing! previous: {previous_score}, newest: {CStarScore}')
        exit(2)
    χ = C[-k:]
    return (D, ℓ, χ, CStarScore)

def Compress(m, k, C, previous_score, previous_ℓ):
    (D, ℓ, χ, new_score) = Dissolve(m, k, C, previous_score, previous_ℓ)
    π = U(D)
    return π + χ, new_score, ℓ

def uparrow(C, μ):
    return [block for block in C if block.level >= μ and block.level_min <= μ]

def getFirstBlockExceedingBlocksPerLevel(C, maximumBlocksPerLevel, ℓ):
    blocksPerLevel = [0] * κ
    firstBlockIndexPerLevel = [None] * κ
    # Could maybe merge both cases if doesn't unoptimize much.
    if KEEPING_ALL_BLOCKS_SINCE_LAST_M_HIGHER_BLOCK:
        # Could maybe optimize by only keeping last `m` blocks height per level but due to Python list implementation I have doubts.
        # Could maybe get rid of `blocksPerLevel` but only use `blocksHeightPerLevel` if don't apply just above optimization.
        # Don't have to first fill data structures and only then check if can remove a block? It seems necessary, for instance if have in this order: 2 * m blocks at level 0, then 2 * m blocks at level 1 (even if this particular case is very unlikely), it won't return the index of the first block at level 0, as have to first ingest half of level 1 blocks and then the considered block height is higher than this middle level 1 block. While it should return the index of the first block at level 0.
        blocksHeightPerLevel = [[]] * κ
        for CIndex, block in enumerate(C):
            for level in range(block.level_min, min(block.level, ℓ) + 1):
                blocksPerLevel[level] += 1
                blocksHeightPerLevel[level] += [C[CIndex].height]
                if firstBlockIndexPerLevel[level] == None:
                    firstBlockIndexPerLevel[level] = CIndex
        for level in range(ℓ):
            firstBlockLevelIndex = firstBlockIndexPerLevel[level]
            if blocksPerLevel[level] > maximumBlocksPerLevel and blocksPerLevel[level + 1] >= m and C[firstBlockLevelIndex].height < blocksHeightPerLevel[level + 1][-m]:
                return firstBlockLevelIndex, level
        return None, None
    else:
        ℓ -= 1
        for CIndex, block in enumerate(C):
            for level in range(block.level_min, min(block.level, ℓ) + 1):
                blocksPerLevel[level] += 1
                if firstBlockIndexPerLevel[level] == None:
                    firstBlockIndexPerLevel[level] = CIndex
                if blocksPerLevel[level] > maximumBlocksPerLevel:
                    return firstBlockIndexPerLevel[level], level
        return None, None

# Proceeding by dichotomy may not be worth it.
# For instance for the first 60 000 blocks, we have in theory 10 levels for the last blocks while log2(256) = 8, so because of first blocks not having a higher than 8 level, it doesn't seem worth it at the beginning at least.
def getℓ(C, previous_ℓ):
    previous_ℓ += 1
    # Can't use `[0] * previous_ℓ`, as would have to check if the array is long enough below before updating its values.
    blocksPerLevel = [0] * κ
    for block in C:
        for level in range(previous_ℓ, block.level + 1):
            blocksPerLevel[level] += 1
    for level in range(previous_ℓ, κ):
        if blocksPerLevel[level] < 2 * m:
            return level - 1

def llbracket(C, μ, CIndex, actually = False):
    removed = False
    block = C[CIndex]
    if block.level == μ:
        if actually:
            debug('removing', block.height)
        del C[CIndex]
        removed = True
    else:
        if actually:
            debug(f'increasing {block.height} level_min from {block.level_min} to {μ + 1}')
        block.level_min += 1
    return removed, block

def score(C, ℓ):
    return sum([(min(block.level, ℓ) + 1 - block.level_min) * block.score for block in C])

def U(chains):
    chains = [chains[μ] for μ in chains]
    flattened_chain = [block for chain in chains for block in chain]
    unique_blocks = {}
    for block in flattened_chain:
        if block.height not in unique_blocks or block.level_min < unique_blocks[block.height].level_min:
            unique_blocks[block.height] = block
    unique_blocks = unique_blocks.values()
    sorted_blocks = sorted(unique_blocks, key=lambda block: block.height)
    return sorted_blocks

lastTimeCheck = None
def printTimeSinceLastTimeCheck(s):
    if verifyCorrectness:
        return
    global lastTimeCheck
    currentTime = time.time()
    if lastTimeCheck is None:
        lastTimeCheck = currentTime
    print(s, round((currentTime - lastTimeCheck) * 1_000, 3), 'ms')
    lastTimeCheck = currentTime

def debug(*args):
    if debugging:
        print(*args)

parser = get_parser() # Create a parser
args = parser.parse_args() # Parse arguments
if args.headers != config.HEADERS_FILE_PATH:
    args.load_from_headers = True
print(args)

# Mining in Logarithmic Space parameters
k = args.unstable_part_length
m = 3 * k

debugging = args.debugging
verifyCorrectness = args.verify_correctness

config.HEADERS_FILE_PATH = args.headers
bitcoin.LOAD_FROM_HEADERS_FILE = args.load_from_headers
bitcoin.DATADIR = args.datadir

lvls = {}

targets = []
compressSize = []
compressScore = []
timestamps = []

previous_score, previous_score_online = 0, 0
previous_ℓ, previous_ℓ_online = 0, 0
C = []
Π = []
headersNumber = bitcoin.loadHeaders(args.break_at)
for height in range(headersNumber):
    #if height >= 428088:
    #    debugging = True
    #    verifyCorrectness = True
    lastTimeCheck = time.time()
    b = bitcoin.getBlockByHeight(height)

    # Not precise at the block scale.
    targets += [b.target]
    compressSize += [len(Π)]
    compressScore += [previous_score]
    timestamps += [b.timestamp]

    #printTimeSinceLastTimeCheck('block retrieval')
    isLastIteration = height == headersNumber - 1
    debugging = debugging or isLastIteration
    verifyCorrectness = verifyCorrectness or isLastIteration
    debug(f'{height=}')
    bLvl = b.level
    for lvl in range(bLvl + 1):
        if lvl in lvls:
            lvls[lvl] += 1
        else:
            lvls[lvl] = 1
    for lvl in lvls:
        debug(f'{lvl}: {lvls[lvl]}')
    debug(f'Before compress block level {bLvl} {b.score}')
    Π, previous_score, previous_ℓ = Compress(m, k, Π + [b], previous_score, previous_ℓ)
    printTimeSinceLastTimeCheck(f'compress computation ({height=})')
    if verifyCorrectness:
        (D, ℓ, χ, previous_score) = Dissolve(m, k, Π, previous_score, previous_ℓ)
        debug(f'After compress (score: {score(Π[:-k], ℓ)}):')
        #printTimeSinceLastTimeCheck('dissolve computation')
        for μ in D:
            debug(bcolors.WARNING + f'μ: {μ}' + bcolors.ENDC + f', len(Π): {len(Π)}, ' + bcolors.OKGREEN + f'len(D[μ]): {len(D[μ])}' + bcolors.ENDC + f' ({[f"{b.height} ({b.level_min} - {b.level}, {round(b.score, 4)})" for b in D[μ]]})')
    if args.verify_online_property:
        C += [b]
        Π_online, previous_score_online, previous_ℓ_online = Compress(m, k, copy.deepcopy(C), previous_score_online, previous_ℓ_online)
        if Π != Π_online:
            print("Online property doesn't hold!")
            for id_, Π, previous_score, previous_ℓ in [
                ['Iterative', Π, previous_score, previous_ℓ],
                ['All of a sudden', Π_online, previous_score_online, previous_ℓ_online]
            ]:
                (D, ℓ, χ, previous_score) = Dissolve(m, k, Π, previous_score, previous_ℓ)
                for μ in D:
                    print(f'{id_}: ' + bcolors.WARNING + f'μ: {μ}' + bcolors.ENDC + f', len(Π): {len(Π)}, ' + bcolors.OKGREEN + f'len(D[μ]): {len(D[μ])}' + bcolors.ENDC + f' ({[f"{b.height} ({b.level_min} - {b.level}, {round(b.score, 4)})" for b in D[μ]]})')
            exit(1)
    #if ℓ == 2:
    #    break
    if args.break_at:
        if height == args.break_at:
            break
    if args.step:
        input()

data = {
    'targets': targets,
    'compressSize': compressSize,
    'compressScore': compressScore,
    'timestamps': timestamps
}

with open('data.json', 'w') as f:
    json.dump(data, f)