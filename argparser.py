import argparse
import config

def get_parser():
    parser = argparse.ArgumentParser(description="Variable MLS on Bitcoin implementation", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--version", action="version", version='%(prog)s 1.0')
    parser.add_argument("-v", "--verbose", action="count", default=0, help="increase output verbosity")

    # Mining in Logarithmic Space parameters
    parser.add_argument("-k", "--unstable-part-length", type=int, metavar="K_PARAMETER",
                        default=6, help="Length of the unstable part of the chain")

    # Toggles
    parser.add_argument("--debugging", action="store_true",
                        help="Turn on debugging")
    parser.add_argument("--verify-correctness", action="store_true",
                        help="Turn on correctness comparison")
    parser.add_argument("--verify-online-property", action="store_true",
                        help="Turn on online property verification")

    # Bitcoin block loading
    parser.add_argument("--load-from-headers", action="store_true", help="Load data from headers")
    parser.add_argument("--headers", type=str, metavar="HEADERS_FILE_PATH", default=config.HEADERS_FILE_PATH, help="Path to the headers")
    parser.add_argument("-d", "--datadir", type=str, metavar="DATADIR_FILE_PATH", default=None, help="Path to bitcoin datadir")

    # Execution
    parser.add_argument("-s", "--step", action="store_true", help="Execute step by step")
    parser.add_argument("-b", "--break-at", type=int, metavar="HEIGHT", default=None, help="Stop execution at specified block height")

    return parser