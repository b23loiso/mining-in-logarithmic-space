from block import Block
import subprocess
import json
import requests
import config

# https://github.com/bitcoin/bitcoin/blob/dfe2dc1d84436d4eae351612dbf0b92f032389be/share/rpcauth/rpcauth.py
serverURL = f'http://{config.user}:{config.passphrase}@localhost:8332'
LOAD_FROM_HEADERS_FILE = False
DATADIR = None

def split_list(alist, wanted_parts):
    length = len(alist)
    return [alist[i*length // wanted_parts: (i+1)*length // wanted_parts] for i in range(wanted_parts)]

def pull(command, params, slices):
    paramsChunks = split_list(params, slices)
    pullResults = []
    for params in paramsChunks:
        payload = [{'method': command, 'params': [param]} for param in params]
        responses = requests.post(serverURL, json=payload)
        data = responses.json()
        results = [response['result'] for response in data]
        pullResults += results
    return pullResults

def loadHeaders(break_at):
    global headers
    print('Loading headers...')
    if not LOAD_FROM_HEADERS_FILE:
        headersNumber = break_at if break_at else int(cli(['getblockcount']))
        headerHashes = pull('getblockhash', list(range(headersNumber)), 2)
        headers = pull('getblockheader', headerHashes, 4)
    else:
        with open(config.HEADERS_FILE_PATH) as f:
            headers = json.load(f)
        headersNumber = break_at if break_at else len(headers)
    print('Headers were loaded!')
    return headersNumber

def getBlockByHeight(height):
    blockHeader = headers[height]
    _hash = int(blockHeader['hash'], 16)
    bits = bits_to_target(int(blockHeader['bits'], 16))
    # How is `mediantime` computed and is it only increasing?
    # Note that `time` isn't only increasing. Cf https://en.bitcoin.it/wiki/Block_timestamp
    timestamp = int(blockHeader['time'])
    block = Block(height, bits, _hash, timestamp)
    return block

def cli(arguments):
    if DATADIR:
        arguments.insert(0, '-datadir=' + DATADIR)
    return subprocess.check_output(['bitcoin-cli'] + arguments).decode('utf-8')[:-1]

def cli_json(arguments):
    return json.loads(cli(arguments))

# Source: https://github.com/dionyziz/popow/blob/9a75ab28deb5f2165856977ba9a1fce6d1301b6f/experiment-electrum/parse_blockchain.py#L17-L26
def bits_to_target(bits):
    bitsN = (bits >> 24) & 0xff
    bitsBase = bits & 0xffffff
    target = bitsBase << (8 * (bitsN - 3))
    return target
