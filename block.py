import math

# bits_to_target(int(headers[0]['bits'], 16))
genesisTarget = 26959535291011309493156476344723991336010898738574164086137773096960
IS_EPSILON_TARGET_DEFINITION = True
MINING_IN_LOGARITHMIC_SPACE_CONSTANT_DIFFICULTY_LEVEL = False

class Block:
    # `target` and `_hash` are `int`s.
    def __init__(self, height, target, _hash, timestamp):
        self.height = height
        self.target = target
        # As penultimate and most minimal difference targets differ only up to the third decimal.
        self.score = round(genesisTarget / target, 4) if IS_EPSILON_TARGET_DEFINITION else genesisTarget - target
        self.level_min = 0
        self.level = level(_hash, target)
        self.timestamp = timestamp

    def __hash__(self):
        return hash((self.height, self.target, self.score, self.level_min, self.level))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __lt__(self, other):
        return self.height < other.height

    def __repr__(self):
        return f"Block({self.height}, {self.target}, {self.score}, {self.level_min}, {self.level})"

    def __str__(self):
        return f"height: {self.height}, target: {self.target}, score: {self.score}, level_min: {self.level_min}, level: {self.level}"


def level(_hash, target):
    ratio = _hash / (genesisTarget if MINING_IN_LOGARITHMIC_SPACE_CONSTANT_DIFFICULTY_LEVEL else target)
    return math.floor(-math.log2(ratio))