from bcolors import bcolors
import time
import bitcoin
import json
from argparser import get_parser
import config
import copy

# Bitcoin parameters
κ = 256

def Dissolve(m, k, C, previous_score, previous_ℓ):
    CStar = C[:-k]
    D = {}
    if len(CStar) >= 2 * m:
        ℓ = getℓ(CStar, previous_ℓ)
        D[ℓ] = uparrow(CStar, ℓ)
        previousCStarμ = D[ℓ]
        for μ in range(ℓ - 1, -1, -1):
            b = previousCStarμ[-m]
            CStarμ = uparrow(CStar, μ)
            CStarμSinceb = [block for block in CStarμ if block.height >= b.height]
            chains = {chainsIndex: chain for chainsIndex, chain in enumerate([CStarμ[-2 * m:], CStarμSinceb])}
            D[μ] = U(chains)
            previousCStarμ = CStarμ
    else:
        ℓ = 0
        D[0] = CStar
    CStarScore = score(CStar, ℓ)
    χ = C[-k:]
    return (D, ℓ, χ, CStarScore)

def Compress(m, k, C, previous_score, previous_ℓ):
    (D, ℓ, χ, new_score) = Dissolve(m, k, C, previous_score, previous_ℓ)
    π = U(0, ℓ, D)
    return π + χ, new_score, ℓ

def uparrow(C, μ):
    chain = copy.deepcopy([block for block in C if block.level >= μ and block.level_min <= μ])
    for blockIndex, block in enumerate(chain):
        block.level_min = μ
    return chain

# Proceeding by dichotomy may not be worth it.
# For instance for the first 60 000 blocks, we have in theory 10 levels for the last blocks while log2(256) = 8, so because of first blocks not having a higher than 8 level, it doesn't seem worth it at the beginning at least.
def getℓ(C, previous_ℓ):
    previous_ℓ += 1
    # Can't use `[0] * previous_ℓ`, as would have to check if the array is long enough below before updating its values.
    blocksPerLevel = [0] * κ
    for block in C:
        for level in range(previous_ℓ, block.level + 1):
            blocksPerLevel[level] += 1
    for level in range(previous_ℓ, κ):
        if blocksPerLevel[level] < 2 * m:
            return level - 1

def score(C, ℓ):
    return sum([(min(block.level, ℓ) + 1 - block.level_min) * block.score for block in C])

def U(lowerBound, upperBound, chains):
    chains = [chains[μ] for μ in chains]
    flattened_chain = [block for chain in chains[lowerBound:upperBound+1] for block in chain]
    unique_blocks = {}
    for block in flattened_chain:
        if not block.height in unique_blocks or block.level_min < unique_blocks[block.height].level_min:
            unique_blocks[block.height] = block
    unique_blocks = unique_blocks.values()
    sorted_blocks = sorted(unique_blocks, key=lambda block: block.height)
    return sorted_blocks

lastTimeCheck = None
def printTimeSinceLastTimeCheck(s):
    if verifyCorrectness:
        return
    global lastTimeCheck
    currentTime = time.time()
    if lastTimeCheck == None:
        lastTimeCheck = currentTime
    print(s, round((currentTime - lastTimeCheck) * 1_000, 3), 'ms')
    lastTimeCheck = currentTime

def debug(*args):
    if debugging:
        print(*args)

parser = get_parser() # Create a parser
args = parser.parse_args() # Parse arguments
if args.headers != config.HEADERS_FILE_PATH:
    args.load_from_headers = True
print(args)

# Mining in Logarithmic Space parameters
k = args.unstable_part_length # TODO: be able to redo Loïc computation
m = 3 * k

debugging = args.debugging
verifyCorrectness = args.verify_correctness

config.HEADERS_FILE_PATH = args.headers
bitcoin.LOAD_FROM_HEADERS_FILE = args.load_from_headers

lvls = {}

targets = []
compressSize = []
compressScore = []
timestamps = []

previous_score = 0
previous_ℓ = 0
Π = []
headersNumber = bitcoin.loadHeaders(args.break_at)
for height in range(headersNumber):
    lastTimeCheck = time.time()
    b = bitcoin.getBlockByHeight(height)

    # Not precise at the block scale.
    targets += [b.target]
    compressSize += [len(Π)]
    compressScore += [previous_score]
    timestamps += [b.timestamp]

    #printTimeSinceLastTimeCheck('block retrieval')
    isLastIteration = height == headersNumber - 1
    debugging = debugging or isLastIteration
    verifyCorrectness = verifyCorrectness or isLastIteration
    debug(f'{height=}')
    bLvl = b.level
    for lvl in range(bLvl + 1):
        if lvl in lvls:
            lvls[lvl] += 1
        else:
            lvls[lvl] = 1
    for lvl in lvls:
        debug(f'{lvl}: {lvls[lvl]}')
    debug(f'Before compress block level {bLvl} {b.score}')
    Π, previous_score, previous_ℓ = Compress(m, k, Π + [b], previous_score, previous_ℓ)
    printTimeSinceLastTimeCheck(f'compress computation ({height=})')
    if verifyCorrectness:
        (D, ℓ, χ, previous_score) = Dissolve(m, k, Π, previous_score, previous_ℓ)
        debug(f'After compress (score: {score(Π[:-k], ℓ)}):')
        #printTimeSinceLastTimeCheck('dissolve computation')
        for μ in D:
            debug(bcolors.WARNING + f'μ: {μ}' + bcolors.ENDC + f', len(Π): {len(Π)}, ' + bcolors.OKGREEN + f'len(D[μ]): {len(D[μ])}' + bcolors.ENDC + f' ({[f"{b.height} ({b.level_min} - {b.level}, {round(b.score, 4)})" for b in D[μ]]})')
    #if ℓ == 2:
    #    break
    if args.break_at:
        if height == args.break_at:
            break
    if args.step:
        input()

data = {
    'targets': targets,
    'compressSize': compressSize,
    'compressScore': compressScore,
    'timestamps': timestamps
}

with open('data.json', 'w') as f:
    json.dump(data, f)